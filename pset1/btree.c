#include <stdlib.h>
#include <stdio.h>
#include "btree.h"

///////////////////////////////////
// Declare b_tree functions //
//////////////////////////////

void bt_insert(node ** tree, void * address, int size) {

	//
	// It's empty tree
	//
	if(!(*tree)) {

        node * tmp = NULL;

        tmp = (node *) malloc(sizeof(node));
        tmp->left = tmp->right = NULL;
        tmp->address = address;
		tmp->size = size;
        *tree = tmp;

        return;
	}

    if(address < (*tree)->address) {
        bt_insert(&(*tree)->left, address, size);
    } else if(address > (*tree)->address) {
        bt_insert(&(*tree)->right, address, size);
    }
}

node* bt_search(node ** tree, void * address) {

	//
	// It's empty tree
	//
    if(!(*tree)) {
        return NULL;
    }

    if(address < (*tree)->address) {
        bt_search(&((*tree)->left), address);
    } else if(address > (*tree)->address) {
        bt_search(&((*tree)->right), address);
    } else if(address == (*tree)->address) {
        return *tree;
    }
}

node* bt_parent(node ** root, node ** child) {

	//
	// It's empty tree
	//
    if((!(*root)) || (!(*child))) {
        return NULL;
    }

    node * tmp = NULL;

    if ((*root)->left == *child || (*root)->right == *child) {
        return *root;
    }

    if ((*root)->left != NULL) {

        tmp = bt_parent(&((*root)->left), &(*child));

        if (tmp != NULL) {
            return tmp;
        }
    }

    if ((*root)->right != NULL) {

        tmp = bt_parent(&((*root)->right), &(*child));

        if (tmp != NULL) {
            return tmp;
        }
    }
}

int bt_length(node * tree) {

	//
	// It's empty tree
	//
    if(!(tree)) {
        return 0;
    }

    int counter = 1;

    if(tree->left) {
        counter += bt_length(tree->left);
    }

    if(tree->right) {
        counter += bt_length(tree->right);
    }

    return counter;
}

