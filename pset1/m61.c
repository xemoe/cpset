#define M61_DISABLE 1
#include "btree.h"
#include "m61.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <inttypes.h>
#include <math.h>
#include <assert.h>

void me_printleakreport_notinheap(void * ptr, const char * file, int line) {
    printf("MEMORY BUG: %s:%d: invalid free of pointer %p, not in heap\n", file, line, &ptr);
    abort();
}

void me_printleakreport_notallocated(void * ptr, const char * file, int line) {
    printf("MEMORY BUG: %s:%d: invalid free of pointer %p, not allocated\n", file, line, &ptr);
    abort();
}

//
// Defined my global variables
//

static m61_statistics global_stats;
static node * meta_stats = NULL;

///////////////////////////
// Start implements //
//////////////////////

void * m61_malloc(size_t sz, const char* file, int line) {

    (void) file, (void) line;   

    //
    // Prevent from invalid allocate size
    //

    if (sz <= 0 || (sz > pow(2,30))) {

        global_stats.nfail += 1;
        global_stats.fail_size += sz;

        return NULL;
    }

    void * ptr = base_malloc(sz);

    //
    // Statistic counter
    //

    global_stats.nactive += 1;
    global_stats.ntotal += 1;
    global_stats.active_size += sz;
    global_stats.total_size += sz;

    //
    // Default set heap min (0)
    // or check new pointer are lower
    //
    if (global_stats.heap_min == 0) {
        global_stats.heap_min = ptr;
    } else if (global_stats.heap_min > (char *) ptr) {
        global_stats.heap_min = ptr;
    }

    //
    // Default set heap max (0)
    // or check new pointer + sz are greater
    //
    if (global_stats.heap_max == 0) {
        global_stats.heap_max = ptr + sz;
    } else if (global_stats.heap_max < (char *) ptr + sz) {
        global_stats.heap_max = ptr + sz;
    }

    //
    // Find if exist memory @ meta_stats
    //
    node * node_size = bt_search(&meta_stats, ptr);

    if (node_size == NULL) {
        bt_insert(&meta_stats, ptr, sz);
    } else {
        //
        // Change allocate size @ meta_stats
        //
        node_size->size = sz;
        node_size->free = false;
    }

    return ptr;
}

void m61_free(void *ptr, const char *file, int line) {

    (void) file, (void) line;  

    if (ptr == NULL) {
        return;
    }

    //
    // Statistic ounter
    //

    node * node_size = bt_search(&meta_stats, ptr);

    //
    // Free memory size from meta_stats
    //
    if (node_size != NULL) {

        if (node_size->free) {
            me_printleakreport_notinheap(ptr, file, line);
        }

        global_stats.nactive -= 1;
        global_stats.active_size -= node_size->size;
        node_size->free = true;
        base_free(ptr);

    } else {

        if (!(((char *) ptr >= global_stats.heap_min) && ((char *) ptr <= global_stats.heap_max))) {
            me_printleakreport_notinheap(ptr, file, line);
        }

        //
        // Free pointer are inside heep-allocated area
        //
        if ((((char *) ptr >= global_stats.heap_min) && ((char *) ptr <= global_stats.heap_max)) || (strcmp(file,__FILE__))) {
            me_printleakreport_notallocated(ptr, file, line);
        }

        me_printleakreport_notinheap(ptr, file, line);
    }
}

void* m61_realloc(void* ptr, size_t sz, const char* file, int line) {

    void* new_ptr = NULL;

    if (sz) {
        new_ptr = m61_malloc(sz, file, line);
    }
    
    if (ptr && new_ptr) {
        memcpy(new_ptr, ptr, sz);
    }

    m61_free(ptr, file, line);

    return new_ptr;
}


/// m61_calloc(nmemb, sz, file, line)
///    Return a pointer to newly-allocated dynamic memory big enough to
///    hold an array of `nmemb` elements of `sz` bytes each. The memory
///    is initialized to zero. If `sz == 0`, then m61_malloc may
///    either return NULL or a unique, newly-allocated pointer value.
///    The allocation request was at location `file`:`line`.

void* m61_calloc(size_t nmemb, size_t sz, const char* file, int line) {

    if (nmemb <= 0 || (nmemb > pow(2,30))) {

        global_stats.nfail += 1;
        global_stats.fail_size += (nmemb * sz);

        return NULL;
    }

    void* ptr = m61_malloc(nmemb * sz, file, line);

    if (ptr) {
        memset(ptr, 0, nmemb * sz);
    }

    return ptr;
}


void m61_getstatistics(struct m61_statistics* stats_pt) {
    memcpy(stats_pt, &global_stats, sizeof(global_stats));
}

void m61_printstatistics(void) {
    printf("malloc count: active %10llu   total %10llu   fail %10llu\n",
           global_stats.nactive, global_stats.ntotal, global_stats.nfail);
    printf("malloc size:  active %10llu   total %10llu   fail %10llu\n",
           global_stats.active_size, global_stats.total_size, global_stats.fail_size);
}


void m61_printleakreport(void) {
    abort();
}

