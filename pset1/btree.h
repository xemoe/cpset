////////////////////////////////
// Declare b_tree struct //
///////////////////////////

typedef int bool;
#define true 1
#define false 0

typedef struct b_tree {
    bool free;
	unsigned long size;
	void * address;
	struct b_tree * right;
	struct b_tree * left;
} node;

///////////////////////////////////
// Declare b_tree functions //
//////////////////////////////

void bt_insert(node ** tree, void * address, int size);
node * bt_search(node ** tree, void * address);
node * bt_parent(node ** root, node ** child);
node * bt_rebance(node ** root);
int bt_length(node * tree);

